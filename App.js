/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet
} from 'react-native';

import {
  Colors  
} from 'react-native/Libraries/NewAppScreen';

import AppNavigator from './src/Navigation/Navigator'
import NavigationService from './src/Navigation/NavigationService';


import { createStore, combineReducers, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';

import cartItemsReducer from './src/store/reducers/CartReducer';

const rootReducer = combineReducers({
  cartItems: cartItemsReducer
}, composeWithDevTools());

const store = createStore(rootReducer);

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <SafeAreaView style={styles.mainContainer}>
          <AppNavigator ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}/>
      </SafeAreaView>    
    </Provider>      
  );
};

const styles = StyleSheet.create({
  mainContainer: {    
    flex:1,
    backgroundColor: Colors.white,
  }
});

export default App;
