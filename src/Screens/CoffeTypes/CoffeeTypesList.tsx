import React from 'react';
import { View, FlatList, StyleSheet, Alert } from 'react-native';
import {NavigationActions} from 'react-navigation';
import navigateToScreen from '../../../App'

import CoffeeTypeListItem from './CoffeeTypeListItem';
import NavigationService  from '../../Navigation/NavigationService';

const CoffeeTypesList = props => {
  const renderCoffeeTypeListItem = itemData => {
    return (
      <CoffeeTypeListItem
        coffeeType = {itemData.item}                
        onCoffeeTypeItemClicked={() => {                      
           NavigationService.navigate('CoffeOrder', {coffeeType : itemData.item});
        }}
      />
    );
  };

  return (
    <View style={styles.list}>
      <FlatList
        data={props.listData}
        keyExtractor={(item, index) => item.id}
        renderItem={renderCoffeeTypeListItem}
        style={styles.coffeeTypesList}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
    width:'100%',    
    backgroundColor:'#faf4ee'
  },
  coffeeTypesList: { 
    flex : 1, 
    width:'100%' 
   }
});

export default CoffeeTypesList;