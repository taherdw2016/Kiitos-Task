import React from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native';

const CoffeeTypeListItem = props => {
  return (
      <View style={styles.coffeeTypeItem}>
          <TouchableOpacity onPress={props.onCoffeeTypeItemClicked}>
              <View style={{ flexDirection: 'row', width:'100%', height:'100%'}}>
                  <View style={styles.coffeeTypeIconContainer}>
                      <Image style={styles.coffeeTypeImage} source={props.coffeeType.image}/>
                  </View>
                  <Text style={styles.coffeeTypeNameContainer}>{props.coffeeType.name }</Text>
                  <View style={styles.rightArrowContainer}>
                      <Image source={require('../../assets/images/right_arrow.png')}/>
                  </View>
              </View>
          </TouchableOpacity>
      </View>
  );
};

const styles = StyleSheet.create({
    coffeeTypeItem: {
        height: 100,
        width: '100%',
        paddingHorizontal:16,
        backgroundColor:'#faf4ee'                     
    },
    coffeeTypeIconContainer: {        
        width: '20%',        
        justifyContent:'center',
        alignItems:'center',       
    },
    coffeeTypeImage: {                                                  
    },
    coffeeTypeNameContainer: {        
        width: '70%',      
        textAlignVertical:'center',  
        marginStart: 8              
    },    
    rightArrowContainer: {        
        width: '10%',
        justifyContent:'center'             
    }    
});

export default CoffeeTypeListItem;