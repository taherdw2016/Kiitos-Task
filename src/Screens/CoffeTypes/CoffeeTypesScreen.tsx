import React, { Component, useState, useEffect } from 'react';
import { View, Text, StyleSheet, Alert, ActivityIndicator, TextPropTypes } from 'react-native';
import CoffeeTypesList from './CoffeeTypesList';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux'
import {addToCartAction} from '../../store/actions/CartItemsActions'
//import CoffeeTypesList from '../CoffeTypes/CoffeeTypesList'

const CoffeeTypesScreen = props  => {

  const [isScreenLoading, setIsScreenLoading] = useState(true)
  const [coffeeTypes, setCoffetTypes] = useState([])

  const CART_ITEMS_KEY = "cart_items";

  const generateCoffeeTypesList = () => {
    let coffeeType = {};
    let coffeeTypesList= [
      {
        id : 1,
        name : "Espresso",
        image:require('../../assets/images/espresso.png'),
        sizePriceMap: 
        {
          "S":5,
          "M":12,
          "L":18,
        }
      }, 
      {
        id : 2,
        name : "Cappuccino",
        image:require('../../assets/images/cappuccino.png'),
        sizePriceMap: 
        {
          "S":4,
          "M":22,
          "L":35,
        }
      }, 
      {
        id : 3,
        name : "Macchiato",
        image:require('../../assets/images/macchiato.png'),
        sizePriceMap: 
        {
          "S":15,
          "M":20,
          "L":30,
        }
      }, 
      {
        id : 4,
        name : "Mocha",
        image:require('../../assets/images/mocha.png'),
        sizePriceMap: 
        {
          "S":6,
          "M":44,
          "L":50,
        }
      }, 
      {
        id : 5,
        name : "Latte",
        image:require('../../assets/images/latte.png'),
        sizePriceMap: 
        {
          "S":13,
          "M":31,
          "L":44,
        }
      }
    ];   
    return coffeeTypesList;
  }
  
const dispatch = useDispatch();

  const getSavedCartItems = async () => {
    let savedCartItems = [];
    try {
      const strSavedCartItems = await AsyncStorage.getItem(CART_ITEMS_KEY)
      if(strSavedCartItems !== null) {        
        savedCartItems = JSON.parse(strSavedCartItems);
      }
    } catch(e) {     
    }
    return savedCartItems;
  }

  useEffect(() => {    
    setTimeout(function () {
      setIsScreenLoading(false);
      setCoffetTypes(generateCoffeeTypesList()); 
    }, 1000);        
    async function getCartItems() {
      dispatch(addToCartAction(await getSavedCartItems()));
    }
    getCartItems();
  }, [])

  const getLoadingView = () => {
    return (
      <View style={styles.screen}>
        <ActivityIndicator />
      </View>
    );
  }

  const getCoffeeTypesScreenMainView = () => {
    return (
      <View style={styles.screen}>
        <Text style={styles.baseText}>
           It's Great
          <Text style={styles.innerText}> Day for Coffee.</Text>
        </Text>
        <CoffeeTypesList listData={coffeeTypes} navigation={props.navigation} />
      </View>
    );
  }

  const createCoffeeTypesScreenView = () => {
    let coffeTypesView = null;
    if (isScreenLoading) {
      coffeTypesView = getLoadingView();

    } else {
      coffeTypesView = getCoffeeTypesScreenMainView();
    }
    return coffeTypesView;
  }

  return (createCoffeeTypesScreenView());

};

const styles = StyleSheet.create({
  screen: {
    flex: 1,       
    backgroundColor: "#faf4ee"  ,    
  },
  baseText: { 
    fontSize:36,
    paddingStart : 16,
    color:'black', 
    fontWeight:'bold'
  },
  innerText: {  
    color:'#b88681'  
  }
});

export default CoffeeTypesScreen;