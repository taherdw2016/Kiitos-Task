import React, { Component, useState, useEffect } from 'react';
import { View, Text, StyleSheet, Alert, ActivityIndicator } from 'react-native';

const LocationScreen = props  => {

  const [isScreenLoading, setIsScreenLoading] = useState(true)
  
  useEffect(() => {
    setTimeout(function () {
      setIsScreenLoading(false);      
    }, 2000);        
  }, [])

  const getLoadingView = () => {
    return (
      <View style={styles.screen}>
        <ActivityIndicator size='large' color='black' />
      </View>
    );
  }

  const getLocationScreenMainView = () => {
    return (
      <View style={styles.screen}>
          <Text style={styles.baseText}>Here is Location screen</Text>
      </View>
    );
  }

  const createLocationScreenView = () => {
    let locationScreenView = null;
    if (isScreenLoading) {
      locationScreenView = getLoadingView();

    } else {
      locationScreenView = getLocationScreenMainView();
    }
    return locationScreenView;
  }

  return (createLocationScreenView());

};

const styles = StyleSheet.create({
  screen: {
    flex: 1,      
    justifyContent:'center',
    alignItems:'center',        
    backgroundColor: "#faf4ee"  ,    
  },
  baseText: { 
    fontSize:30,
    justifyContent:'center',
    alignItems:'center',
    color:'black'  
  },
  activityIndicatorContainer:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  }
});

export default LocationScreen;