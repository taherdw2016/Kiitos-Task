import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import CartItem from './CartItem';

const CartItemsList = props => {
  const renderCartListItem = itemData => {
    return (
      <CartItem
      cartItems = {props.listData}
      cartItem = {itemData.item}        
      />
    );
  };

  return (
    <View style={styles.list}>
      <FlatList
        data={props.listData}
        keyExtractor={(item, index) => item.id}
        renderItem={renderCartListItem}
        style={styles.cartItemsList}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  list: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
    width:'100%',    
    backgroundColor:'#faf4ee'
  },
  cartItemsList: { 
    flex : 1, 
    width:'100%' 
   }
});

export default CartItemsList;