import React, { Component, useState, useEffect } from 'react';
import { View, Text, StyleSheet, Alert, ActivityIndicator, TextPropTypes } from 'react-native';
import CartItemsList from './CartItemsList';
import {useSelector} from 'react-redux'

const CartScreen = props  => {
  const [isScreenLoading, setIsScreenLoading] = useState(false)

  const retrivedCartItems = useSelector(state => state.cartItems.cartItemsList);

  if (retrivedCartItems.length === 0){
    return(<View style={{flex:1,justifyContent:'center', alignItems:'center', backgroundColor:'#faf4ee'}}>
      <Text style={{fontSize:20}}>No Items in cart yet</Text>
    </View>);
  }

  const getLoadingView = () => {
    return (
      <View style={styles.screen}>
        <ActivityIndicator />
      </View>
    );
  }

  const getCartScreenMainView = () => {
    return (
      <View style={styles.screen}>        
        <CartItemsList listData={retrivedCartItems} navigation={props.navigation} />
      </View>
    );
  }

  const createCartScreenView = () => {
    let coffeTypesView = null;
    if (isScreenLoading) {
      coffeTypesView = getLoadingView();

    } else {
      coffeTypesView = getCartScreenMainView();
    }
    return coffeTypesView;
  }

  return (createCartScreenView());

};

const styles = StyleSheet.create({
  screen: {
    flex: 1,       
    backgroundColor: "#faf4ee"  ,    
  },
  baseText: { 
    fontSize:36,
    paddingStart : 16,
    color:'black', 
    fontWeight:'bold'
  },
  innerText: {  
    color:'#b88681'  
  }
});

export default CartScreen;