import React , {useState} from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

const CartItem = props => {
  const cartItems = props.cartItems
  const cartItem = props.cartItem;
  
  const CART_ITEMS_KEY = "cart_items";
  const [coffeeQuantity, setCoffeeQuantity] = useState(cartItem.itemsQuantity)
  const ADD_COFFEE_ITEM = "ADD";
  const REMOVE_COFFEE_ITEM = "REMOVE";

   const getCartItemImage = () => 
   {
    let cartItemImage = null;
    switch(cartItem.name)
    {        
        case "Espresso":
            cartItemImage = require('../../assets/images/espresso.png');
        break;

        case "Cappuccino":
            cartItemImage = require('../../assets/images/cappuccino.png');
        break;

        case "Macchiato":
            cartItemImage = require('../../assets/images/macchiato.png');
        break;

        case "Mocha":
            cartItemImage = require('../../assets/images/mocha.png');
        break;

        case "Latte":
            cartItemImage = require('../../assets/images/latte.png');
        break;
    }
    return cartItemImage;
   }

   const getCartItemDetails = () => 
   {
    let cartItemDetails = "";
    switch(cartItem.chosenSize)
    {
         case "S":
            cartItemDetails = "Small, ";
         break;

         case "M":
            cartItemDetails = "Meduim, ";
         break;

         case "L":
            cartItemDetails = "Large, ";
         break;         
    }
    
    switch(cartItem.chosenSugarAmount)
    {
        case 0:
            cartItemDetails += "no sugar";
        break;

        case 1:
            cartItemDetails += "1 cube";
        break;

        case 2:
            cartItemDetails += "2 cubes";
        break;

        case 3:
            cartItemDetails += "3 cubes";
        break;
    }
    return cartItemDetails;
   }

   const saveCartItems = async (cartItems) => {
    try {
      const cartItemsAsJson = JSON.stringify(cartItems)
      await AsyncStorage.setItem(CART_ITEMS_KEY, cartItemsAsJson)
    } catch (e) {
      // saving error
      let error = 0;
    }
  }

   const updateSavedCartItemQuantity = async (quantity) =>{     
     let toBeSavedCartItem = cartItem; 
     let toBeSavedCartItems = cartItems;
    let resultCartItems = cartItems.filter(function(savedCartItem) { 
        let toBeRetunedCartItem = null;
        if (savedCartItem.id === cartItem.id && 
          savedCartItem.chosenSize === cartItem.chosenSize && 
          savedCartItem.chosenSugarAmount === cartItem.chosenSugarAmount){
            toBeRetunedCartItem = savedCartItem;
          }
          return toBeRetunedCartItem;
       });  
       
       if (resultCartItems.length > 0){
         toBeSavedCartItem.itemsQuantity = quantity;
         let index = cartItems.indexOf(resultCartItems[0]);
         toBeSavedCartItems[index] = toBeSavedCartItem;           
         await saveCartItems(toBeSavedCartItems);         
       }              
   }

   const changeCoffeeQuantity = async (action:string) => {
    let newCoffeeQuantity = 0;
    if (action === ADD_COFFEE_ITEM){
        newCoffeeQuantity = coffeeQuantity + 1;
        setCoffeeQuantity(coffeeQuantity + 1)
    } else if(action === REMOVE_COFFEE_ITEM){
        newCoffeeQuantity = coffeeQuantity === 0 ? 0 : coffeeQuantity - 1;        
    }
    setCoffeeQuantity(newCoffeeQuantity);
    await updateSavedCartItemQuantity(newCoffeeQuantity);
  }

  return (
      <View style={styles.coffeeTypeItem}>          
              <View style={{ flexDirection: 'row'}}>
                <TouchableOpacity style={{ flexDirection: 'row', width:'90%', height:'100%'}} onPress={props.onCoffeeTypeItemClicked}>
                  <View style={styles.coffeeTypeIconContainer}>
                      <Image style={styles.coffeeTypeImage} source={getCartItemImage()}/>
                  </View>
                  <View style={styles.cartItemDetailsContainer}>
                    <Text>{cartItem.name }</Text>
                    <Text>{getCartItemDetails()}</Text>    
                  </View>                  
                </TouchableOpacity>
                <View style={styles.addOrRemoveItemsViewsContainer}>
                  <TouchableOpacity style={[styles.addOrRemoveItemTouchableOpacity, {borderTopStartRadius:50, borderTopEndRadius:50}]}
                onPress={changeCoffeeQuantity.bind(this, ADD_COFFEE_ITEM)}>
                <Text style={styles.addOrRemoveItemTouchableOpacityText}>+</Text>
            </TouchableOpacity>
            <View style={styles.coffeeQuantityContainer}>
                <Text style={styles.coffeeQuantity}>{coffeeQuantity}</Text>
            </View>
            <TouchableOpacity style={[styles.addOrRemoveItemTouchableOpacity, {borderBottomStartRadius:50, borderBottomEndRadius:50}]}
            onPress={changeCoffeeQuantity.bind(this, REMOVE_COFFEE_ITEM)}>
            <Text style={styles.addOrRemoveItemTouchableOpacityText}>-</Text>
            </TouchableOpacity>
        
                  </View>
              </View>          
      </View>
  );
};

const styles = StyleSheet.create({
    coffeeTypeItem: {
        height: 100,
        width: '100%',
        paddingHorizontal:16,
        backgroundColor:'#faf4ee'                     
    },
    coffeeTypeIconContainer: {        
        width: '20%',        
        justifyContent:'center',
        alignItems:'center',       
    },
    coffeeTypeImage: {                                                  
    },
    cartItemDetailsContainer: {        
        width:'70%', 
        justifyContent:'center', 
        marginStart:8             
    },    
    addOrRemoveItemsViewsContainer: {        
        width: '10%',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:8

    },
    addOrRemoveItemTouchableOpacity:{   
      alignItems:'center', 
      justifyContent:'center',
      width:'100%',     
      backgroundColor:'#ba8679' 
    },
    addOrRemoveItemTouchableOpacityText:{  
      color:'white', 
      fontSize:25  
    },
    coffeeQuantityContainer:{ 
      width:'100%',  
      justifyContent:'center', 
      alignItems:'center'   
    },
    coffeeQuantity:{
      color:'black'    
    },    
});

export default CartItem;