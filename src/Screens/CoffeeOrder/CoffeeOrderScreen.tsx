import React, { Component, useState, useEffect, useCallback } from 'react';
import { View, Image, Text, StyleSheet, Alert, ActivityIndicator, TouchableOpacity, ImageBackground, Button } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux'
import {addToCartAction} from '../../store/actions/CartItemsActions'

const CoffeeOrderScreen = props  => {
const CART_ITEMS_KEY = "cart_items";

 const SMALL_SIZE = "S";
 const MEDUIM_SIZE = "M";
 const LARGE_SIZE = "L";

 const UNSELECTED_OPACITY = 0.4;
 const SELECTED_OPACITY = 1;

 const ADD_COFFEE_ITEM = "ADD";
 const REMOVE_COFFEE_ITEM = "REMOVE";

  const { getParam } = props.navigation;
  const [isScreenLoading, setIsScreenLoading] = useState(true)
  const coffeeType  = getParam('coffeeType');
  const [coffeeName, setCoffeName] = useState(coffeeType.name)
  const [coffeeQuantity, setCoffeeQuantity] = useState(1)
  const [coffePrice, setCoffeePrice] = useState(coffeeType.sizePriceMap["M"])
  const [coffeSize, setCoffeSize] = useState("M")
  const [sugarAmount, setSugarAmount] = useState(0)

  const [coffeSizeItemsOpacities, setCoffeSizeItemsOpacities] = useState(new Map())

   let cartItems = [];

  const dispatch = useDispatch();

  useEffect(() => {
    setIsScreenLoading(false);  
    let opacitiesMap = new Map();
    opacitiesMap.set(SMALL_SIZE, UNSELECTED_OPACITY);
    opacitiesMap.set(MEDUIM_SIZE, SELECTED_OPACITY);
    opacitiesMap.set(LARGE_SIZE, UNSELECTED_OPACITY);
    setCoffeSizeItemsOpacities(opacitiesMap);    
  }, [])

  const getLoadingView = () => {
    return (
      <View style={styles.activityIndicatorContainer}>
        <ActivityIndicator size='large' color='black' />
      </View>
    );
  }

  const changeCoffeeQuantity = (action:string) => {
    let newCoffeeQuantity = 0;
    if (action === ADD_COFFEE_ITEM){
        newCoffeeQuantity = coffeeQuantity + 1;
        setCoffeeQuantity(coffeeQuantity + 1)
    } else if(action === REMOVE_COFFEE_ITEM){
        newCoffeeQuantity = coffeeQuantity === 0 ? 0 : coffeeQuantity - 1;        
    }
    setCoffeeQuantity(newCoffeeQuantity);
    setCoffeePrice(coffeeType.sizePriceMap[coffeSize] * newCoffeeQuantity)
  }

  const chooseSize = (size : string) => {
      setCoffeSize(size);
      setCoffeePrice(coffeeType.sizePriceMap[size] * coffeeQuantity);

      let opacitiesMap = new Map();
      [...opacitiesMap.keys()].forEach((key) => {
        if (key === size){
            opacitiesMap.set(key, SELECTED_OPACITY);
        } else {
            opacitiesMap.set(key, UNSELECTED_OPACITY);
        }        
      });       
      setCoffeSizeItemsOpacities(opacitiesMap)
  }

  const getOpacity = (size) => {    
    return coffeSizeItemsOpacities.get(size);
  }

  const chooseSugarAmount = (sugarAmount : number) => {
    setSugarAmount(sugarAmount);
  }

  const getSavedCartItems = async () => {
    let savedCartItems = [];
    try {
      const strSavedCartItems = await AsyncStorage.getItem(CART_ITEMS_KEY)
      if(strSavedCartItems !== null) {        
        savedCartItems = JSON.parse(strSavedCartItems);
      }
    } catch(e) {     
    }
    return savedCartItems;
  }

  const saveCartItems = async (cartItems) => {
    try {
      const cartItemsAsJson = JSON.stringify(cartItems)
      await AsyncStorage.setItem(CART_ITEMS_KEY, cartItemsAsJson)
    } catch (e) {      
    }
  }

  const addToCart = async () => { 
    let cartItem = coffeeType;
    cartItem.chosenSize = coffeSize;
    cartItem.chosenSugarAmount = sugarAmount;   
    cartItem.itemsQuantity = coffeeQuantity;

    cartItems = await getSavedCartItems();  
    let resultCartItems = cartItems.filter(function(savedCartItem) { 
      let toBeRetunedCartItem = null;
      if (savedCartItem.id === cartItem.id && 
        savedCartItem.chosenSize === cartItem.chosenSize && 
        savedCartItem.chosenSugarAmount === cartItem.chosenSugarAmount){
          toBeRetunedCartItem = savedCartItem;
        }
        return toBeRetunedCartItem;
     });  
     if (resultCartItems.length > 0){
       cartItem.itemsQuantity += resultCartItems[0].itemsQuantity;
       let index = cartItems.indexOf(resultCartItems[0]);
       cartItems[index] = cartItem;
     } else {
      cartItems.push(cartItem);
     }         
    saveCartItems(cartItems);      
    dispatch(addToCartAction(cartItems));
    props.navigation.goBack();    
  }

  const getCoffeeOrderScreenMainView = () => {
    return (
    <View style={styles.screen}> 
        <ImageBackground resizeMode='cover' style={{width:'100%', height:250}} source={require('../../assets/images/pattern_background.png')}>
            <View style={{flexDirection:'row',justifyContent:'space-around', alignItems:'center', width:'100%', height:60, paddingHorizontal:16}}> 
            <TouchableOpacity style={{width:'10%', height:'100%',flexDirection:'row', justifyContent:'flex-start', alignItems:'center'}}
            onPress={() => props.navigation.goBack()}>
                <Image source={require('../../assets/images/back_icon.png')}/>
            </TouchableOpacity>  
            <View style={{width:'90%', height:'100%', flexDirection:'row' ,justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontSize:16}}>{coffeeName}</Text>           
            </View>                      
            </View>
            <View style={{justifyContent:'center', alignItems:'center', marginTop:8}}>
                <Image resizeMode='contain' style={{width:100, height:100}} source={coffeeType.image} />
            </View>
        </ImageBackground>
        <View style={styles.coffeeNameContainer}>
            <View style={styles.coffeeTextContainer}>
                <Text style={styles.coffeeName}>{coffeeName}</Text>
                </View>
            <TouchableOpacity style={[styles.addOrRemoveItemTouchableOpacity, {borderTopStartRadius:50, borderBottomStartRadius:50}]}
                onPress={changeCoffeeQuantity.bind(this, REMOVE_COFFEE_ITEM)}>
                <Text style={styles.addOrRemoveItemTouchableOpacityText}>-</Text>
            </TouchableOpacity>
            <View style={styles.coffeeQuantityContainer}>
                <Text style={styles.coffeeQuantity}>{coffeeQuantity}</Text>
            </View>
            <TouchableOpacity style={[styles.addOrRemoveItemTouchableOpacity, {borderTopEndRadius:50, borderBottomEndRadius:50}]}
            onPress={changeCoffeeQuantity.bind(this, ADD_COFFEE_ITEM)}>
            <Text style={styles.addOrRemoveItemTouchableOpacityText}>+</Text>
            </TouchableOpacity>
        </View>
        <View><Text style={styles.coffeePrice}>{coffePrice}</Text></View>         
        <Text style={styles.sizeTitle}>Size</Text>
        <View style={styles.coffeeSizesContainer}>
            <TouchableOpacity style={[styles.coffeeSizesItemContainer,{opacity:getOpacity(SMALL_SIZE)}]} onPress={chooseSize.bind(this,SMALL_SIZE)}>
                <Image resizeMode='contain' style={{height: 28, width: 28}} source={coffeeType.image}/>
            </TouchableOpacity>        
            <TouchableOpacity style={[styles.coffeeSizesItemContainer,{opacity:getOpacity(MEDUIM_SIZE)}]} onPress={chooseSize.bind(this,MEDUIM_SIZE)}>
                <Image resizeMode='contain' style={{height: 36, width: 36}}  source={coffeeType.image}/>
            </TouchableOpacity> 
            <TouchableOpacity style={[styles.coffeeSizesItemContainer,{opacity:getOpacity(LARGE_SIZE)}]} onPress={chooseSize.bind(this,LARGE_SIZE)}>
                <Image resizeMode='contain' style={{height: 48, width: 48}} source={coffeeType.image}/>
            </TouchableOpacity>                     
        </View>
        <Text style={styles.sugarBaseTitle}>
            Sugar
            <Text style={styles.sugarInnerTitle}> (In Cubes)</Text>
        </Text>
        <View style={styles.sugarItemsContainer}>
        <TouchableOpacity style={styles.sugarItemContainer} onPress={chooseSugarAmount.bind(this,0)}>
            <Image style={[styles.sugarImage, {tintColor:'gray'}]}  source={require('../../assets/images/zero_sugar_cube_icon.png')}/>
        </TouchableOpacity>        
        <TouchableOpacity style={styles.sugarItemContainer} onPress={chooseSugarAmount.bind(this,1)}>
            <Image style={[styles.sugarImage, {tintColor:'black'}]} source={require('../../assets/images/one_sugar_cube_icon.png')}/>
        </TouchableOpacity> 
        <TouchableOpacity style={styles.sugarItemContainer} onPress={chooseSugarAmount.bind(this,2)}>
            <Image style={[styles.sugarImage, {tintColor:'gray'}]} source={require('../../assets/images/three_sugar_cubes_icon.png')}/>
        </TouchableOpacity>        
        <TouchableOpacity style={styles.sugarItemContainer} onPress={chooseSugarAmount.bind(this,3)}>
            <Image style={[styles.sugarImage, {tintColor:'gray'}]} source={require('../../assets/images/three_sugar_cubes_icon.png')}/>
        </TouchableOpacity> 
    </View>
        <TouchableOpacity style={styles.addToCart} onPress={addToCart.bind(this)}>
            <Text style={{color:'white'}}>Add to cart</Text>
        </TouchableOpacity>
    </View>
    );
  }

  const createCoffeeOrderScreenView = () => {
    let coffeeOrderScreenView = null;
    if (isScreenLoading) {
      coffeeOrderScreenView = getLoadingView();

    } else {
      coffeeOrderScreenView = getCoffeeOrderScreenMainView();
    }
    return coffeeOrderScreenView;
  }

  return (createCoffeeOrderScreenView());

};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "white"  ,    
  },
  activityIndicatorContainer:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  coffeeNameContainer:{ 
    flexDirection:'row', 
    height:40, 
    marginTop:16, 
    marginHorizontal:16   
  },
  coffeeTextContainer:{ 
    width:'70%', 
    justifyContent:'center',
     alignItems:'flex-start'  
  },
  coffeeName:{ 
    fontSize:30   
  },
  addOrRemoveItemTouchableOpacity:{   
    alignItems:'center', 
    justifyContent:'center',
    width:'10%',     
    backgroundColor:'#ba8679' 
  },
  addOrRemoveItemTouchableOpacityText:{  
    color:'white', 
    fontSize:25  
  },
  coffeeQuantityContainer:{ 
    width:'10%',  
    justifyContent:'center', 
    alignItems:'center'   
  },
  coffeeQuantity:{
    color:'black'    
  },
  coffeePriceContainer:{        
  },
  coffeePrice:{
    fontSize:24,
     color:'#ba8679',
      marginHorizontal:16, 
      marginTop:8, 
      fontWeight:'bold'
  },
  sizeTitle:{ 
    fontSize:20, 
    color:'#ddd4d4', 
    marginHorizontal:16,
    marginTop:8, 
    fontWeight:'bold'   
  },
  coffeeSizesContainer:{
    flexDirection:'row', 
    justifyContent:'flex-start', 
    alignItems:'baseline',
    marginHorizontal:16, 
    marginTop:8
  },
  coffeeSizesItemContainer:{    
    marginHorizontal:16    
  },
  sugarBaseTitle:{  
    fontSize:20,
    color:'#ddd4d4', 
    marginHorizontal:16, 
    marginTop:8, 
    fontWeight:'bold'  
  },
  sugarInnerTitle:{ 
    fontSize:16, 
    color:'#d4d1d2'   
  },
  sugarItemsContainer:{ 
    flexDirection:'row', 
    justifyContent:'flex-start', 
    alignItems:'baseline', 
    marginHorizontal:16, 
    marginTop:16  
  },
  sugarItemContainer:{
    marginHorizontal:16
  },
  sugarImage:{
    width:24, 
    height:24
  },
  addToCart:{
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center', 
    marginTop:32,
    width:150, 
    height:50, 
    borderRadius:50, 
    backgroundColor:'#ba8679',
    shadowColor: 'black', // IOS
    shadowOffset: { height: 1, width: 1 }, // IOS
    shadowOpacity: 1, // IOS
    shadowRadius: 1, //IOS
    elevation: 8, // Android
  }
});

export default CoffeeOrderScreen;