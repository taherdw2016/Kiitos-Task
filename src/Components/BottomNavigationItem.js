import React from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  TouchableOpacity
} from 'react-native';

const BottomNavigationItem = props => {

    const getBottomNavigationIcon = (routeName) => 
    {
        let focused = props.isIconFocused;
        let bottomNavigationItemImageSource = require('../assets/images/home_icon.png');
        if (routeName === 'Home') {
            bottomNavigationItemImageSource = focused
              ? require('../assets/images/selected_home_icon.png')
              : require('../assets/images/home_icon.png');            
          } else if (routeName === 'Location') {
            bottomNavigationItemImageSource = focused
              ? require('../assets/images/selected_location_icon.png')
              : require('../assets/images/location_icon.png');
          }
          else if (routeName === 'Cart') {
            bottomNavigationItemImageSource = focused
            ? require('../assets/images/selected_cart_icon.png')
            : require('../assets/images/cart_icon.png');
          }
          else if (routeName === 'Profile') {
            bottomNavigationItemImageSource = focused
            ? require('../assets/images/selected_profile_icon.png')
            : require('../assets/images/profile_icon.png');
          }
          return bottomNavigationItemImageSource;
    }


  return (     
      <Image source={getBottomNavigationIcon(props.name)}/>
  );
};

const styles = StyleSheet.create({
    coffeeTypeItem: {
        height: 100,
        width: '100%',
        paddingHorizontal:16,
        backgroundColor:'#faf4ee'                     
    }
});

export default BottomNavigationItem;