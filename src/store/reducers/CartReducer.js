import { ADD_TO_CART } from '../actions/CartItemsActions';

const initialState = {    
    cartItemsList: []
  };

  const cartItemsReducer = (state = initialState, action) => {    
    switch (action.type) {
        case ADD_TO_CART:
            let updatedCartItems = action.cartItems;             
            return {cartItemsList:updatedCartItems};       
        default:
          return state;
      }      
  };
  
  export default cartItemsReducer;