export const ADD_TO_CART = 'ADD_TO_CART';

export const addToCartAction = (updatedCartItems) => {
    return { type: ADD_TO_CART, cartItems: updatedCartItems };
};