import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import BottomNavigationItem from '../Components/BottomNavigationItem'

import CoffeeTypesScreen from '../Screens/CoffeTypes/CoffeeTypesScreen';
import CoffeeOrderScreen from '../Screens/CoffeeOrder/CoffeeOrderScreen';
import LocationScreen from '../Screens/Location/LocationScreen';
import CartScreen from '../Screens/Cart/CartScreen';
import ProfileScreen from '../Screens/Profile/ProfileScreen';

const HomeNavigator = createStackNavigator(
    {
      CoffeeTypes: CoffeeTypesScreen ,
      CoffeOrder: CoffeeOrderScreen      
    }, {
      initialRouteName:'CoffeeTypes',
      headerMode: 'none'
    }
  );

  const LocationNavigator = createStackNavigator(
    {
      Location: LocationScreen      
    }, {
      initialRouteName:'Location',
      headerMode: 'screen'
    }
  );

  const CartNavigator = createStackNavigator(
    {
      Cart: CartScreen      
    }, {
      initialRouteName:'Cart',
      headerMode: 'screen'
    }
  ); 

  const CoffeOrderNavigator = createStackNavigator(
    {
      CoffeOrder: CoffeeOrderScreen
    }, {
      initialRouteName:'Cart',
      headerMode: 'none'
    }
  ); 

  const ProfileNavigator = createStackNavigator(
    {
      Profile: ProfileScreen      
    }, {
      initialRouteName:'Profile',
      headerMode: 'screen'
    }
  );

  const BottomNavigator = createBottomTabNavigator(
    {
     Home : HomeNavigator,
     Location : LocationNavigator,
     Cart : CartNavigator, 
     Profile : ProfileNavigator,   
    },
    {
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
          const { routeName } = navigation.state;          
          // You can return any component that you like here!
          return <BottomNavigationItem name={routeName} isIconFocused = {focused} size={25} color={tintColor} />;
        },
      }),
      tabBarOptions: {
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      },
    }    
  );

  const MainNavigator = createStackNavigator(
    {
      Tabs:BottomNavigator,
      CoffeOrder: CoffeOrderNavigator      
    }, {
      initialRouteName:'Tabs',
      headerMode: 'none'
    }
  ); 

  export default createAppContainer(MainNavigator);